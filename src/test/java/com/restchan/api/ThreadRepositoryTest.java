package com.restchan.api;

import com.avaje.ebean.Ebean;
import com.restchan.RestChanApplication;
import com.restchan.RestChanConfiguration;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.api.Assertions.assertThat;

public class ThreadRepositoryTest {

    @ClassRule
    public static final DropwizardAppRule<RestChanConfiguration> RULE =
            new DropwizardAppRule<>(RestChanApplication.class, "test.yml");

    private final ThreadRepository threadRepository = new ThreadRepository(Ebean.getServer("default"));

    public Thread newThread() {
        Post newPost = new Post("Hello, World!");
        List<Post> posts = new ArrayList<>();
        posts.add(newPost);
        Thread newThread = new Thread(posts);

        return newThread;
    }

    @Test
    public void testCreate() throws Exception {
        Thread newThread = newThread();

        threadRepository.create(newThread);

        assertThat(newThread.getId()).isNotNull();
        assertThat(newThread.getPosts().size()).isEqualTo(1);
        assertThat(newThread.getPosts().get(0).getContent()).isEqualTo("Hello, World!");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCannotCreateWithoutAPost() {
        Thread newThread = new Thread();

        threadRepository.create(newThread);
    }

    @Test
    public void testSave() {
        Thread newThread = newThread();

        newThread = threadRepository.save(newThread);

        assertThat(newThread.getId()).isNotNull();
    }

    @Test
    public void testAll() throws Exception {
        for(int i = 0; i < 5; ++i) {
            Thread newThread = newThread();
            threadRepository.save(newThread);
        }

        List<Thread> threads = threadRepository.all();
        assertThat(threads.size()).isEqualTo(5);
    }

    @Test
    public void testFind() throws Exception {
        Thread newThread = newThread();
        threadRepository.save(newThread);

        Thread retrievedThread = threadRepository.find(newThread.getId());

        assertThat(retrievedThread.getId()).isEqualTo(newThread.getId());
    }

}
