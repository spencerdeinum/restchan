package com.restchan.resources;

import com.restchan.api.Thread;
import com.restchan.api.ThreadRepository;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class ThreadResourceTest {

    private static final ThreadRepository threadRepository = mock(ThreadRepository.class);
    private static final ThreadResource threadResource = new ThreadResource(threadRepository);

    private Thread thread = new Thread();

    @Before
    public void setup() {
        thread.setId(1);
        when(threadRepository.find(1)).thenReturn(thread);
    }

    public void testIndex() throws Exception {

    }

    @Test(expected = WebApplicationException.class)
    public void testThatCreateFailsWithoutAFirstPost() throws Exception {
        Thread newThread = new Thread();

        when(threadRepository.create(newThread)).thenThrow(IllegalArgumentException.class);

        threadResource.create(newThread);
    }

    @Test
    public void testGet() throws Exception {
        assertThat(threadResource.get(1)).isEqualsToByComparingFields(thread);

        verify(threadRepository).find(1);
    }
}
