package com.restchan.resources;

import com.restchan.api.Thread;
import com.restchan.api.ThreadRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/thread")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ThreadResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThreadResource.class);

    private final ThreadRepository threadRepository;

    public ThreadResource(ThreadRepository threadRepository) {
        this.threadRepository = threadRepository;
    }

    @GET
    public List<Thread> index() {
        List<Thread> threads = threadRepository.all();

        return threads;
    }

    @POST
    public Thread create(Thread thread) {
        try {
            thread = threadRepository.create(thread);
        } catch(IllegalArgumentException ex) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }

        return thread;
    }

    @GET
    @Path("{id}")
    public Thread get(@PathParam("id") long id) {

        LOGGER.info("Searching for Thread with id: " + id);

        Thread thread = threadRepository.find(id);
        if(thread == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        return thread;
    }
}
