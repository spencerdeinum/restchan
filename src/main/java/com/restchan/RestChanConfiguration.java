package com.restchan;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class RestChanConfiguration extends Configuration {

    @Valid
    @NotNull
    @JsonProperty("database")
    private EbeanFactory ebeanFactory = new EbeanFactory();

    @JsonProperty("database")
    public EbeanFactory getEbeanFactory() {
        return ebeanFactory;
    }

}
