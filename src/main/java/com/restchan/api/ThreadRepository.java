package com.restchan.api;

import com.avaje.ebean.EbeanServer;

import java.util.List;

public class ThreadRepository {

    private final EbeanServer ebean;

    public ThreadRepository(EbeanServer ebeanServer) {
        this.ebean = ebeanServer;
    }

    public Thread create(Thread thread) {
        if(thread.getPosts() == null || thread.getPosts().size() != 1) {
            throw new IllegalArgumentException("New Threads should contain one new post");
        }
        ebean.save(thread);
        return thread;
    }

    public List<Thread> all() {
        return ebean.find(Thread.class).findList();
    }

    public Thread find(long id) {
        return ebean.find(Thread.class, id);
    }

    public Thread save(Thread newThread) {
        ebean.save(newThread);
        return newThread;
    }
}
