package com.restchan.api;

import com.avaje.ebean.validation.NotEmpty;
import com.fasterxml.jackson.annotation.JsonBackReference;
import io.dropwizard.jackson.JsonSnakeCase;

import javax.persistence.*;

@Entity
@Table(name = "posts")
@JsonSnakeCase
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty
    private String content;

    @ManyToOne
    @JsonBackReference
    private Thread thread;

    public Post() {}

    public Post(String content) {
        this.content = content;
    }

    public Post(Thread thread, String content) {
        this.thread = thread;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Thread getThread() {
        return thread;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }
}
