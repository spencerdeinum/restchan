package com.restchan.api;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.dropwizard.jackson.JsonSnakeCase;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "threads")
@JsonSnakeCase
public class Thread {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToMany(cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Post> posts;

    public Thread() {}

    public Thread(List<Post> posts) {
        this.posts = posts;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public List<Post> getPosts() {
        return posts;
    }
}
