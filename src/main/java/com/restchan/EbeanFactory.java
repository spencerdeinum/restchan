package com.restchan;

import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.EbeanServerFactory;
import com.avaje.ebean.config.DataSourceConfig;
import com.avaje.ebean.config.ServerConfig;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.restchan.api.Post;
import io.dropwizard.setup.Environment;
import org.hibernate.validator.constraints.NotEmpty;

public class EbeanFactory {

    @NotEmpty
    private String driverClass;

    @NotEmpty
    private String user;

    private String password;

    @NotEmpty
    private String url;

    @JsonProperty
    public String getDriverClass() {
        return driverClass;
    }

    @JsonProperty
    public void setDriverClass(String driverClass) {
        this.driverClass = driverClass;
    }

    @JsonProperty
    public String getUser() {
        return user;
    }

    @JsonProperty
    public void setUser(String user) {
        this.user = user;
    }

    @JsonProperty
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty
    public String getUrl() {
        return url;
    }

    @JsonProperty
    public void setUrl(String url) {
        this.url = url;
    }

    public EbeanServer build(Environment environment) {
        final ServerConfig serverConfig = new ServerConfig();
        serverConfig.setDefaultServer(true);
        serverConfig.setName("default");
        serverConfig.addPackage("com.restchan.api");

        serverConfig.setDdlGenerate(true);
        serverConfig.setDdlRun(true);

        /**
         * Register at entities with Ebean
         */
        serverConfig.addClass(com.restchan.api.Thread.class);
        serverConfig.addClass(Post.class);

        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDriver(driverClass);
        dataSourceConfig.setUsername(user);
        dataSourceConfig.setPassword(password);
        dataSourceConfig.setUrl(url);

        serverConfig.setDataSourceConfig(dataSourceConfig);

        EbeanServer ebeanServer = EbeanServerFactory.create(serverConfig);

        return ebeanServer;
    }
}
