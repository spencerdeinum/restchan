package com.restchan.health;

import com.avaje.ebean.Ebean;
import com.codahale.metrics.health.HealthCheck;
import com.restchan.api.Thread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatabaseHealthCheck extends HealthCheck {

    private final Logger logger = LoggerFactory.getLogger(DatabaseHealthCheck.class);

    @Override
    protected Result check() throws Exception {
        try {
            Ebean.find(Thread.class).findList();
            return Result.healthy();
        }
        catch(Exception ex) {
            logger.error(ex.getMessage());
        }

        return Result.unhealthy("Could not execute test query.");
    }
}
