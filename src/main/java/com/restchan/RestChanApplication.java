package com.restchan;

import com.avaje.ebean.EbeanServer;
import com.restchan.api.ThreadRepository;
import com.restchan.health.DatabaseHealthCheck;
import com.restchan.resources.ThreadResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class RestChanApplication extends Application<RestChanConfiguration> {

    public static void main(String[] args) throws Exception {
        new RestChanApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<RestChanConfiguration> restChanConfigurationBootstrap) {
    }

    @Override
    public void run(RestChanConfiguration restChanConfiguration, Environment environment) throws Exception {
        EbeanServer ebeanServer = restChanConfiguration.getEbeanFactory().build(environment);

        environment.jersey().register(new ThreadResource(new ThreadRepository(ebeanServer)));

        environment.healthChecks().register("database", new DatabaseHealthCheck());
    }
}
